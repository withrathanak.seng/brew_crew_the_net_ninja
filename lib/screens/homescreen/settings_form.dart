import 'package:brew_crew_the_net_ninja/models/current_user.dart';
import 'package:brew_crew_the_net_ninja/services/database.dart';
import 'package:brew_crew_the_net_ninja/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:brew_crew_the_net_ninja/shared/constants.dart';
import 'package:provider/provider.dart';

class SettingsForm extends StatefulWidget {
  @override
  _SettingsFormState createState() => _SettingsFormState();
}

class _SettingsFormState extends State<SettingsForm> {

  final _formKey = GlobalKey<FormState>();
  final List<String> sugars = ['0', '1', '2', '3', '4'];

  String _currentName;
  String _currentSugars;
  int _currentStrength;

  @override
  Widget build(BuildContext context) {

    final currentUser = Provider.of<CurrentUser>(context);

    return StreamBuilder<UserData>(
      stream: DatabaseService(uid: currentUser.uid).userData,
      builder: (context, snapshot) {
        if (snapshot.hasData) {

          UserData userData = snapshot.data;

          return Form(
            key: this._formKey,
            child: Column(
              children: [
                Text(
                  'Update your brew settings',
                  style: TextStyle(
                    fontSize: 18.0,
                  ),
                ),
                SizedBox(height: 20.0),
                TextFormField(
                  decoration: textInputDecoration.copyWith(
                    hintText: 'Name',
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.brown[200],
                        width: 2.0,
                      ),
                    ),
                  ),
                  initialValue: userData.name,
                  validator: (value) => value.isEmpty ? 'Please Enter a name' : null,
                  onChanged: (value) => setState(() => this._currentName = value),

                ),
                SizedBox(height: 20.0),
                DropdownButtonFormField(
                  decoration: textInputDecoration.copyWith(
                    hintText: 'Sugars',
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.brown[200],
                        width: 2.0,
                      ),
                    ),
                  ),
                  value: this._currentSugars ?? userData.sugars,
                  items: this.sugars.map((sugar) {
                    return DropdownMenuItem(
                      value: sugar,
                      child: Text(
                        '$sugar sugars',
                      ),
                    );
                  }).toList(),
                  onChanged: (value) => setState(() => this._currentSugars = value),
                ),
                SizedBox(height: 20.0),
                Slider(
                  value: (this._currentStrength ?? userData.strength).toDouble(),
                  activeColor: Colors.brown[this._currentStrength ?? userData.strength],
                  inactiveColor: Colors.brown[this._currentStrength ?? userData.strength],
                  min: 100,
                  max: 900,
                  divisions: 8,
                  onChanged: (value) => setState(() => this._currentStrength = value.round()),
                ),
                SizedBox(height: 20.0),
                RaisedButton(
                  onPressed: () async {
                    if (this._formKey.currentState.validate()) {
                      await DatabaseService(uid: currentUser.uid).updateUserData(
                          this._currentSugars ?? userData.sugars,
                          this._currentName ?? userData.name,
                          this._currentStrength ?? userData.strength
                      );
                      Navigator.pop(context);
                    }
                  },
                  color: Colors.brown[400],
                  child: Text(
                    'Update',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            ),
          );
        } else {
          return Loading();
        }
      }
    );
  }
}
