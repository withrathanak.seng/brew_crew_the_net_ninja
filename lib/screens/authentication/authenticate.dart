import 'package:brew_crew_the_net_ninja/screens/authentication/register.dart';
import 'package:brew_crew_the_net_ninja/screens/authentication/sign_in.dart';
import 'package:flutter/material.dart';


class Authenticate extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<Authenticate> {

  bool showSignIn = true;

  void toggleView() {
    setState(() {
      this.showSignIn = !this.showSignIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    return showSignIn ? SignIn(toggleView: this.toggleView) : Register(toggleView: toggleView);
  }
}
