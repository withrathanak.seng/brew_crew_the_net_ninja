import 'package:brew_crew_the_net_ninja/shared/constants.dart';
import 'package:brew_crew_the_net_ninja/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:brew_crew_the_net_ninja/services/auth.dart';

class Register extends StatefulWidget {

  final Function toggleView;

  Register({this.toggleView});

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {

  final AuthService _auth = AuthService();
  final _formKey = GlobalKey<FormState>();

  bool isLoading = false;

  String email = '';
  String password = '';

  String errorMessage = '';

  @override
  Widget build(BuildContext context) {
    return this.isLoading ? Loading() : Scaffold(
      backgroundColor: Colors.brown[100],
      appBar: AppBar(
        backgroundColor: Colors.brown[400],
        elevation: 0.0,
        title: Text(
            'Sign Up to Brew Crew'
        ),
        actions: [
          FlatButton.icon(
            onPressed: () {
              widget.toggleView();
            },
            icon: Icon(
              Icons.person,
              color: Colors.white,
            ),
            label: Text(
              'Sign In',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
      body: Container(
        // padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
        padding: EdgeInsets.fromLTRB(20.0, 150.0, 20.0, 20.0),
        child: Form(
          key: this._formKey,
          child: Column(
            children: [
              SizedBox(height: 20.0),
              TextFormField(
                validator: (value) => value.isEmpty ? 'Enter an Email' : null,
                onChanged: (value) {
                  setState(() {
                    this.email = value;
                  });
                },
                decoration: textInputDecoration.copyWith(
                  hintText: 'Email',
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.brown[200],
                      width: 2.0,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10.0),
              TextFormField(
                validator: (value) => value.length < 6 ? 'Enter a password 6+ chars long' : null,
                onChanged: (value) {
                  setState(() {
                    this.password = value;
                  });
                },
                obscureText: true,
                decoration: textInputDecoration.copyWith(
                  hintText: 'Password',
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.brown[200],
                      width: 2.0,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10.0),
              RaisedButton(
                onPressed: () async {
                  if (this._formKey.currentState.validate()) {
                    setState(() {
                      this.isLoading = true;
                    });
                    dynamic result = await this._auth.registerWithEmailAndPassword(this.email, this.password);
                    if (result == null) {
                      setState(() {
                        this.isLoading = false;
                        this.errorMessage = 'please input a valid email and password';
                      });
                    }
                  }
                },
                color: Colors.brown[200],
                child: Text(
                  'Register',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              SizedBox(height: 20.0),
              Text(
                '${this.errorMessage}',
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 14.0,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
