import 'package:brew_crew_the_net_ninja/models/current_user.dart';
import 'package:brew_crew_the_net_ninja/screens/authentication/authenticate.dart';
import 'package:brew_crew_the_net_ninja/screens/homescreen/home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final currentUser = Provider.of<CurrentUser>(context) ?? null;

    return currentUser == null ? Authenticate() : Home();
  }
}
