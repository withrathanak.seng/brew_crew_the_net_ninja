import 'package:brew_crew_the_net_ninja/models/current_user.dart';
import 'package:brew_crew_the_net_ninja/services/database.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthService {

  final FirebaseAuth _auth = FirebaseAuth.instance;

  // create User object base on Firebase's user
  CurrentUser _userFromFirebaseUser(User user) {
    return user != null ? CurrentUser(uid: user.uid) : null;
  }

  // auth changes user stream
  Stream<CurrentUser> get user {
    return _auth.authStateChanges().map((User user) => _userFromFirebaseUser(user));
  }

  // sign-in anonymously
  Future signInAnon() async {
    try {
      UserCredential userCredential = await _auth.signInAnonymously();
      return _userFromFirebaseUser(userCredential.user);
    } catch (exception) {
      print(exception.toString());
      return null;
    }
  }

  // sign-in with email and password
  Future signInWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(email: email, password: password);
      return _userFromFirebaseUser(userCredential.user);
    } catch (exception) {
      print(exception.toString());
      return null;
    }
  }

  // register with email and password
  Future registerWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential userCredential = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      // create a new document for the user with uid
      await DatabaseService(uid: userCredential.user.uid).updateUserData('0', 'new crew member', 100);
      return _userFromFirebaseUser(userCredential.user);
    } catch (exception) {
      print(exception.toString());
      return null;
    }
  }

  // sign-out
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (exception) {
      print(exception.toString());
      return null;
    }
  }

}