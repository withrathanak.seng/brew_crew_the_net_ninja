import 'package:brew_crew_the_net_ninja/models/current_user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:brew_crew_the_net_ninja/models/brew.dart';

class DatabaseService {

  // collection reference
  final CollectionReference brewCollection = FirebaseFirestore.instance.collection('brews');
  final String uid;

  DatabaseService({this.uid});

  // get brew stream
  List<Brew> _brewListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.docs.map((doc) {
      return Brew(
        name: doc.data()['name'] ?? '',
        sugars: doc.data()['sugars'] ?? '0',
        strength: doc.data()['strength'] ?? 0,
      );
    }).toList();
  }
  Stream<List<Brew>> get brews {
    return this.brewCollection.snapshots().map(this._brewListFromSnapshot);
  }

  // get a current user brew stream
  UserData _userDataFormSnapshot(DocumentSnapshot snapshot) {
    return UserData(
      uid: snapshot.id,
      name: snapshot.data()['name'],
      sugars: snapshot.data()['sugars'],
      strength: snapshot.data()['strength'],
    );
  }
  Stream<UserData> get userData {
    return this.brewCollection.doc(this.uid).snapshots().map(this._userDataFormSnapshot);
  }

  // update a user document
  Future updateUserData(String sugars, String name, int strength) async {
    return await this.brewCollection.doc(this.uid).set({
      'sugars': sugars,
      'name': name,
      'strength': strength,
    });
  }

}